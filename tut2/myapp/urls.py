from django.urls import path
from . import views


urlpatterns = [
    # ex: /polls/index
    path('/index/', views.index, name='/index/'),
    path('/like/', views.like, name='/like/'),

]
